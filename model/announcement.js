const mongoose = require("mongoose");

const announcementSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Add an announcement"],
  },
  para: {
    type: String,
    required: [true, "Add some contents"],
  },
});

const announcement = mongoose.model("Announcement", announcementSchema);
module.exports = announcement;
