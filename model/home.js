const mongoose = require("mongoose");

const newTournamentSchema = new mongoose.Schema({
  tournamentName: {
    type: String,
    required: [true, "Please tell us your teamname!"],
  },
  durations: {
    type: String,
    required: [true, "what is the durations"],
  },
  numberofTeams: {
    type: Number,
    required: [true, "number of teams!"],
  },
  date: {
    type: String,
    required: [true, "starting date"],
  },
  status: {
    type:String,
    required: [true, "status"],
    default: "Ongoing"
  },
  photo: {
    type: String,
    default: "default.jpg",
  },
  price: {
    type: String,
    required: [true, "price"],
  },
  formate: {
    type: String,
    required: [true, "formate"],
  },
  paragraph: {
    type: String,
    required: [true, "paragraph"],
  },
});

const matches = mongoose.model("matches", newTournamentSchema);
module.exports = matches;
