<<<<<<< HEAD
const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const teams = require("./teams");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please tell us your name!"],
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, "Please provide a valid email"],
  },
  password: {
    type: String,
    required: [true, "Please provide a password!"],
    minlength: 8,
    select: false,
  },
  teams:{
    type: mongoose.Schema.Types.ObjectId, ref: 'Teams',
    default: null,
  },

  passwordConfirm: {
    type: String,
    required: [true, "Please confirm password"],
    validate: {
      validator: function (el) {
        return el === this.password;
      },
      message: "Passwords do not match",
    },
  },
});
=======
// const mongoose = require("mongoose");
// const validator = require("validator");
// const bcrypt = require("bcryptjs");

// const userSchema = new mongoose.Schema({
//   name: {
//     type: String,
//     required: [true, "Please tell us your name!"],
//   },
//   email: {
//     type: String,
//     unique: true,
//     lowercase: true,
//     validate: [validator.isEmail, "Please provide a valid email"],
//   },
//   password: {
//     type: String,
//     required: [true, "Please provide a password!"],
//     minlength: 8,
//     select: false,
//   },
//   passwordConfirm: {
//     type: String,
//     required: [true, "Please confirm password"],
//     validate: {
//       validator: function (el) {
//         return el === this.password;
//       },
//       message: "Passwords do not match",
//     },
//   },
// });
>>>>>>> 25a0c768e99e42318583662e98ccda63caa733ae

// userSchema.pre("save", async function (next) {
//   if (!this.isModified("password")) return next();
//   this.password = await bcrypt.hash(this.password, 12);
//   this.passwordConfirm = undefined;
//   next();
// });

// userSchema.methods.correctPassword = async function (candidatePassword) {
//   return await bcrypt.compare(candidatePassword, this.password);
// };

<<<<<<< HEAD
const User = mongoose.model("User", userSchema);
module.exports=User;

<<<<<<< HEAD
=======
=======
// const User = mongoose.model("User", userSchema);
// module.exports=User;
// 4
>>>>>>> 25a0c768e99e42318583662e98ccda63caa733ae
>>>>>>> 40059d3ac0a1053f7b1dd5b164c1e26e883315a8
