const mongoose = require("mongoose");

const streamSchema = new mongoose.Schema({
  team1: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team',
    required: [true, "Please tell us your team name!"],
  },
  team2: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team',
    required: [true, "Please tell us your team name!"],
  },
  streamLink: {
    type: String,
    required: [true, "Please provide the stream link"],
  },
});

const Stream = mongoose.model("Stream", streamSchema);
module.exports = Stream;
