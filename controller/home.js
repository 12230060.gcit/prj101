const Home = require("./../model/home");

exports.getAlltournament = async (req, res, next) => {
    try {
        const matches = await Home.find();
        res.status(200).json({ data: matches, status: "success" });
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
}

exports.createTournament = async (req, res, next) => {
    try {
        console.log(req.body)
        const matches = await Home.create(req.body);
        res.status(200).json({ data: matches, message: "Home created" })
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
}

exports.getTournament = async (req, res, next) => {
    try {
        const matches = await Home.findById(req.params.id);
        res.status(200).json({ data: matches, status: 'succes' })
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
}

exports.deleteTournament = async (req, res, next) => {
    try {
        const matches = await Home.findByIdAndDelete(req.params.id)
        res.status(200).json({ data: matches, status: 'success' })
    } catch (error) {
        res.status(500).json({ error: error.message })


    }
}








