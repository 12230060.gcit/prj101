// const User = require("./../model/usermodel");
// const path = require("path");
// const jwt = require("jsonwebtoken");
// const AppError = require("./../utils/appError");

// const { promisify } = require("util");

// exports.getAllUser = async (req, res, next) => {
//   try {
//     const user = await User.find();
//     res.status(200).json({ data: user, status: "success" });
//   } catch (error) {
//     res.status(500).json({ error: error.message });
//   }
// };

// // exports.createUser = async (req, res, next) => {
// //   try {
// //     const user = await User.create(req.body);
// //     res.status(200).json({ data: user, message: "User created" });
// //   } catch (error) {
// //     console.log(error);
// //     res.status(500).json({ error: error.message });
// //   }
// // };
// exports.createUser = async (req, res, next) => {
//   try {
//     const { name, email, password, passwordConfirm } = req.body;

//     // Log incoming data for debugging
//     console.log("Received data:", { name, email, password, passwordConfirm });

//     if (!name || !email || !password || !passwordConfirm) {
//       return res.status(400).json({ error: "All fields are required" });
//     }

//     if (password !== passwordConfirm) {
//       return res.status(400).json({ error: "Passwords do not match" });
//     }

//     const user = await User.create({ name, email, password, passwordConfirm });
//     res.status(200).json({ data: user, message: "User created" });
//   } catch (error) {
//     console.error("Error creating user:", error);
//     res.status(500).json({ error: error.message });
//   }
// };

// exports.getUser = async (req, res, next) => {
//   try {
//     const user = await User.findById(req.params.id).populate({
//       path: "partOfGroup",
//       populate: "groupTitle",
//     });
//     res.status(200).json({ data: user, status: "succes" });
//   } catch (error) {
//     res.status(500).json({ error: error.message });
//   }
// };

// exports.updateUser = async (req, res, next) => {
//   try {
//     const { id } = req.params;
//     const { email, ...updatedFields } = req.body;

//     if (email) {
//       const existingUser = await User.findOne({ email });
//       if (existingUser && existingUser._id.toString() !== id) {
//         return res.status(400).json({ error: "Email already exists" });
//       }
//     }

//     const user = await User.findByIdAndUpdate(id, updatedFields, { new: true });

//     res.status(200).json({ data: user, status: "success" });
//   } catch (error) {
//     res.status(500).json({ error: error.message });
//   }
// };

// exports.deleteUser = async (req, res, next) => {
//   try {
//     const user = await User.findByIdAndDelete(req.params.id);
//     res.status(200).json({ data: user, status: "success" });
//   } catch (error) {
//     res.status(500).json({ error: error.message });
//   }
// };

// const signToken = (id) => {
//   return jwt.sign({ id }, process.env.JWT_SECRET, {
//     expiresIn: process.env.JWT_EXPIRES_IN,
//   });
// };

// const createSendToken = (user, statusCode, res) => {
//   const token = signToken(user._id);
//   const cookieOptions = {
//     expires: new Date(
//       Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 1000
//     ),
//     httpOnly: true,
//   };
//   res.cookie("jwt", token, cookieOptions);
//   res.status(statusCode).json({
//     status: "Success",
//     token,
//     data: {
//       user,
//     },
//   });
// };

// exports.logout = (req, res) => {
//   res.cookie("token", "", {
//     expires: new Date(Date.now() - 10 * 1000),
//     httpOnly: true,
//   });
//   res.status(200).json({ status: "success" });
// };

// exports.login = async (req, res, next) => {
//   try {
//     const { email, password } = req.body;

//     if (!email || !password) {
//       return next(new AppError("Please provide an email and password", 400));
//     }

//     const user = await User.findOne({ email }).select("+password");

//     if (!user || !(await user.correctPassword(password, user.password))) {
//       return next(new AppError("Incorrect email or password", 401));
//     }

//     createSendToken(user, 200, res);
//   } catch (err) {
//     res.status(500).json({ error: err.message });
//   }
// };

// exports.protect = async (req, res, next) => {
//   try {
//     let token;

//     if (
//       req.headers.authorization &&
//       req.headers.authorization.startsWith("Bearer")
//     ) {
//       token = req.headers.authorization.split(" ")[1];
//     } else if (req.cookies.jwt) {
//       token = req.cookies.jwt;
//     }

//     if (!token) {
//       return next(new AppError("You are not logged in!! Please Log in", 401));
//     }

//     const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
//     const freshUser = await User.findById(decoded.id);

//     if (!freshUser) {
//       return next(
//         new AppError("The user belonging to this token no longer exists", 401)
//       );
//     }

//     req.user = freshUser;
//     next();
//   } catch (err) {
//     return next(new AppError("Invalid token. Please log in again",401));
// }
// };
