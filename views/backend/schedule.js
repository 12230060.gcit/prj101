document.addEventListener("DOMContentLoaded", () => {
    const getSchedule = async() => {
      try{
        const res = await axios.get("http://localhost:6969/Admin_Scheduledb")
        showSchedule(res.data.data)
      }catch(err){
        alert(err.message)
      }
    }
  
    const showSchedule = async(Schedule) => {
      var ScheduleContainer = document.querySelector(".scheduleContainer")
      getSchedule.forEach(element => {
        ScheduleContainer.innerHTML += `<div class="schedule">
    
        <div class="team">
            <img src="img/teamLogo.png" alt="">
            <p>${element.team1}</p>
        </div>
        <p>VS</p>
        <div class="team">
            <p>${element.team2}</p>
            <img src="img/teamLogo2.png" alt="">
        </div>
    </div>`
      });
    }
    getSchedule()
  
    const saveBtn = document.getElementById('saveBtn')
    saveBtn.addEventListener('click', (e) => {
      e.preventDefault()
      const title = document.getElementById('title').value
      const para = document.getElementById('para').value
      addAnnouncement(title, para)
    })
    const addAnnouncement = async (title, para) => {
      try {
        const res = await axios.post("http://localhost:6969/announcementdb", {
          title,
          para
        });
        if (res.status === 200) {
          alert("Announcement Added Successfully!");
          getAnnouncement();
        }
      } catch (err) {
        alert(err.message);
      }
    };
    
  })