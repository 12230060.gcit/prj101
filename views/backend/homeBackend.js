document.addEventListener("DOMContentLoaded", () => {
  const getTournament = async () => {
    try {
      const res = await axios.get("http://localhost:6969/tournamentdb");
      showTournament(res.data.data);
    } catch (err) {
      alert(err.message);
    }
  };

  const showTournament = async (home) => {
    console.log(home);
    var homeContainer = document.querySelector(".container");
    home.forEach((element) => {
      homeContainer.innerHTML += ` 

        <div class="card">
        <div class="imgWrapper">
            <img src="/img/image 11.svg" alt="" >
        </div>
        <div class="cardContent">
            <h2>${element.tournamentName}</h2>
            <div class="cardDetails">
                 <p>Teams:Status:${element.status}</p>
                <p>Teams:${element.numberofTeams}</p>
                <p>Date:${element.date}</p>
                <p>Duration:${element.durations}</p>
            </div>
            <div class="linkWrapper">
                <a href="/html/overview.html" style="color: green; text-decoration: none;">View Details</a>
            </div>
            
        </div>
    </div>`;
    });
  };
  getTournament();

  const saveBtn = document.getElementById("saveBtn");
  saveBtn.addEventListener("click", (e) => {
    e.preventDefault();
    const tournamentName = document.getElementById("tournamentName").value;
    console.log("Hello");
    const durations = document.getElementById("durations").value;
    console.log("Hello");
    const numberofTeams = document.getElementById("numberofTeams").value;
    const date = document.getElementById("date").value;
    const photo = document.getElementById("photo").value;
    console.log(tournamentName, durations, numberofTeams, date, photo);
    // Enter validation function call or code here

    //

    addTournament(tournamentName, durations, numberofTeams, date, photo);
  });

  const addTournament = async (
    tournamentName,
    durations,
    numberofTeams,
    date,
    photo
  ) => {
    try {
      const res = await axios.post("http://localhost:6969/tournamentdb", {
        tournamentName,
        durations,
        numberofTeams,
        date,
        photo,
      });
      console.log(res);
      if (res.data.message === "Home created") {
        alert("Tournament Added Successfully!");
        getTournament();
      }
    } catch (err) {
      console.log("Bad messafge");
      alert(err.message);
    }
  };
});
