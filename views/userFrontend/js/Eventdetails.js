document.addEventListener("DOMContentLoaded", () => {
  const getTournamentDetail = async () => {
    try {
      // Parse the URL to extract the homeId parameter
      const urlParams = new URLSearchParams(window.location.search);
      const tournamentid = urlParams.get("tournamentid").toString();
      console.log(tournamentid);
      // Now you ca n use the homeId to make the request
      const res = await axios.get(
        `http://localhost:6969/tournamentdb/${tournamentid}`
      );
      showTournament(res.data.data);
    } catch (err) {
      console.log(err.message);
    }
  };

  const showTournament = (Information) => {
    console.log(Information.tournamentName);
    const eventName = document.querySelector(".top");
    const memberInfo = document.querySelector(".paragraph");
    const dateInfo = document.querySelector(".para");
    dateInfo.innerHTML = `
            <p>
                ${Information.paragraph}
            </p>
        `;
    eventName.innerHTML = Information.tournamentName;
    memberInfo.innerHTML = `
      <p><span class="material-symbols-outlined">groups</span> numberofTeams: ${Information.numberofTeams}</p>
      <p><span class="material-symbols-outlined">emoji_events</span> NU: ${Information.price}</p>
      <p><span class="material-symbols-outlined">schedule</span> Duration: ${Information.durations}</p>
      <p><span class="material-symbols-outlined">calendar_month</span> Date: ${Information.date}</p>
      <p><span class="material-symbols-outlined">account_tree</span> Format: ${Information.formate}</p>
    `;
    Information.textContent = Information.description;

    const registerBtn = document.getElementById("registerBtn");
    registerBtn.addEventListener("click", () => {
      window.location.href = "/register";
    });
  };

  getTournamentDetail();
});

var obj;

if (document.cookie) {
  obj = JSON.parse(document.cookie.substring(6));
} else {
  obj = JSON.parse("{}");
}

const urlParams = new URLSearchParams(window.location.search);
const tournamentid = urlParams.get("tournamentid").toString();

console.log(tournamentid);
document.getElementById("registerBtn").addEventListener("click", (event) => {
  event.preventDefault();
  const confirmed = confirm(
    "Are you sure you want to register for this tournament?"
  );
  if (confirmed) {
    console.log(tournamentid); // Assuming tournamentid is defined somewhere
    // Using id manually
    createTourn(tournamentid, "6648837209b7efeb779c3b03");
    // createTourn(tournamentid, document._id);
  } else {
    // User clicked cancel, do nothing
  }
});

async function createTourn(tournamentId, userId) {
  const url = "http://localhost:6969/regesteredtournaments";
  const data = {
    tournmentId: tournamentId,
    userId: userId,
  };

  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw new Error("Failed to register tournament");
    }

    const result = await response.json();
    console.log("Tournament registered successfully:", result);
  } catch (error) {
    console.error("Error registering tournament:", error.message);
  }
}
