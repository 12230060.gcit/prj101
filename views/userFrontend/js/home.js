document.addEventListener("DOMContentLoaded", () => {
    const getAlltournament = async () => {
      try {
        const res = await axios.get("http://localhost:6969/tournamentdb");
        console.log(res.data.data);
        showTournament(res.data.data);
      } catch (err) {
        alert(err.message);
      }
    };
  
    const showTournament = async (home) => {
      var event_cards = document.querySelector(".event_cards");
      home.forEach((home) => {
        event_cards.innerHTML += `
          <a href="/register?tournamentid=${home._id}">
            <div class="card">
              <img src="/img/image 11.svg" alt="">
              <h3>${home.tournamentName}</h3>
              <div class="card-body">
                <div class="card-body0">
                  <p>Status: ${home.status}</p>
                  <p>Date: ${home.date}</p>
                </div>
                <div class="card-body1">
                  <p>Teams: ${home.numberofTeams}</p>
                  <p>Durations: ${home.durations}</p>
                </div>
              </div>
            </div>
          </a>
        `;
      });
      
    };
  
    getAlltournament();
  });
  