const { getTournamentSchedulePage } = require("../../../controller/viewController")

document.addEventListener("DOMContentLoaded", () => {
    const getTournamentLivestream = async() => {
        try{
            const res = await axios.get("http://localhost:6969/Admin_Scheduledb")
            console.log(res.data.data)
            showSchedule(res.data.data)

        }catch(err){
            alert(err.message)
        }
    }

    const showSchedule = async(schedule) =>{
        var scheduleContainer = document.querySelector('.scheduleContainer')
        schedule.forEach(element => {
            scheduleContainer.innerHTML += 
            `  <div class="schedule">
            <div class="team">
                <img src="img/teamLogo.png" alt="">
                <p>${element.team1}</p>
            </div>
            <p>VS</p>
            <div class="team">
                <p>${element.team2}</p>
                <img src="img/teamLogo2.png" alt="">
            </div>
        </div>`
        })
    }

    getTournamentSchedule();
})