// Function to update user's name
async function updateName() {
    const name = document.getElementById("name").value;
  
    try {
      const response = await axios.patch("/userdb/updateProfile", { name });
  
      if (response.data.status === "Success") {
        // Update the UI or show a success message
        console.log("Name updated successfully");
        showSuccess("Name updated successfully")
      } else {
        // Handle error response
        console.error("Failed to update name:", response.data.message);
        showError(response.data.message)
      }
    } catch (error) {
      console.error("Error:", error);
      showError(error)
    }
  }
  
  // Function to change user's password
  async function changePassword() {
    const currentPassword = document.getElementById("password-current").value;
    const newPassword = document.getElementById("password").value;
    const confirmPassword = document.getElementById("password-confirm").value;
  
    try {
      const response = await axios.patch("/userdb/changePassword", {
        currentPassword,
        password: newPassword,
        passwordConfirm: confirmPassword,
      });
  
      if (response.data.status === "Success") {
        // Update the UI or show a success message
        console.log("Password changed successfully");
        showSuccess("Password changed successfully");

      } else {
        // Handle error response
        console.error("Failed to change password:", response.data.message);
        showError("Failed to change password:", response.data.message);
      }
    } catch (error) {
      console.error("Error:", error);
      showError("Couldn't change the password", );
    }
  }
  
  // Add event listeners to the form submit buttons
  document.getElementById("updateForm").addEventListener("submit", function (event) {
    event.preventDefault(); // Prevent default form submission
    updateName(); // Call updateName function
  });
  
  document.getElementById("changePasswordForm").addEventListener("submit", function (event) {
    event.preventDefault(); // Prevent default form submission
    changePassword(); // Call changePassword function
});
