const express = require('express')
const adminController = require('../controller/admin')
const router = express.Router()

router.post('/signup', adminController.signup)
router.post('/login', adminController.login)
router.get('/logout', adminController.logout)

router
    .route('/')
    .get(adminController.getAllAdmins)

router
    .route('/:id')
    .get(adminController.getAdmin)
    .patch(adminController.updateAdmin)
    .delete(adminController.deleteAdmin)

    module.exports=router